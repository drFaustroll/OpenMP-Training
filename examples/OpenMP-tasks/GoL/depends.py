import matplotlib
import numpy as np
import pylab as pl
import sys
import math
from matplotlib.collections import PatchCollection
import matplotlib.lines as mlines

MAXTIME=0.0


files=["results/omp.out","results/task2.out","results/task.out"]

for i in files:
  data = pl.loadtxt(i)
  min_t = min(data[:,2])
  data[:,3] = data[:,3]-min_t
  MAXTIME = max(MAXTIME,max(data[:,3]))

width = 0.5

colours=["red","blue","limegreen","m","darkturquoise","orange"]

for infile in files:
  data = pl.loadtxt( infile )
  
  nrow = int(max(data[:,1]))+1
  min_t = min(data[:,2])
  data[:,2] = data[:,2] - min_t
  data[:,3] = data[:,3] - min_t
  delta_t = max(data[:,3])
  
  barriers = (data[:,1]==-1).any()
  
  fig, ax = matplotlib.pyplot.subplots(figsize=(6,10.7), dpi=510)
  
  if(barriers):
    for i in range(len(data)):
      if(data[i][1] == -1):
        ax.plot([0,nrow],[data[i][2],data[i][2]],'k--',lineWidth=1)
  
        
  
  for i in range(len(data)):
    if(data[i][1] != -1):
      rect = matplotlib.patches.Rectangle([ data[i][1] + 0.5 - width/2, data[i][2] ], width ,(data[i][3]-data[i][2]), fc=colours[int(data[i][0])%len(colours)])
      ax.add_patch(rect)
  
  
  if(not barriers):
    for i in range(len(data)):
      if(data[i][0] > 0):
        for j in range(i):
          if((data[i][0] == data[j][0]+1) and ( (data[i][1] == (data[j][1]-1) % nrow) or (data[i][1] == data[j][1]) or ( data[i][1] == (data[j][1]+1) %nrow) ) ):
            y,x = np.array([[data[j][3],data[i][2] ],[data[j][1]+0.5,data[i][1]+0.5]])
            line=mlines.Line2D(x,y,lw=.5,alpha=1.0, color=colours[int(data[j][0])%len(colours)])
            ax.add_line(line)
  
  
  
  ax.set_xlim([-0.2, nrow+0.2])
  ax.set_ylim([MAXTIME*1.01,0.0])
  #9x16 aspect ratio
  outfile = infile[infile.find('/')+1:infile.find('.')]
  outfile = outfile+'.png'
  fig.savefig(outfile)
#  matplotlib.pyplot.show()



