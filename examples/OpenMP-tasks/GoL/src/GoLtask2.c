#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <omp.h>

#ifndef HEIGHT
  #define HEIGHT 2048
#endif
#ifndef WIDTH
  #define WIDTH 2048
#endif
#ifndef TIMESTEPS
  #define TIMESTEPS 256
#endif

double world_start;

void output_board(char **board, int width, int height){
  for(int x = 0; x < height; x++){
    for(int y = 0; y < width; y++){
      if(board[x][y]){
          printf("1 ");
      }else{
          printf("0 ");
      }
    }
    printf("\n");
  }
  printf("\n\n");
  fflush(stdout);
}

void evolve_step(char **board, char **newboard, int width, int height, int timesteps, char **depends , FILE* fp){
  if(timesteps> 0){
    for(int x = 0; x < height; x++){
      /* Each task computes a single row */
      /* Each task depends on neighbouring rows being computed for the previous timestep */
      #pragma omp task firstprivate(x) depend(in: depends[timesteps-1][(x-1+height)%height],depends[timesteps-1][x],depends[timesteps-1][(x+1)%height]) depend(out:depends[timesteps][x])
      {
#ifdef TIMING
        double start = omp_get_wtime()-world_start;
#endif
      for(int y = 0; y < width; y++){
        int n = 0;
        for(int x1 = x-1; x1 <= x+1; x1++){
          for(int y1 = y-1; y1 <= y+1; y1++){
            int tempx = x1, tempy = y1;
            /* Here we check for the edge cases*/
            if(tempy < 0) tempy += width;
            if(tempy >= width) tempy -= width;
            if(tempx < 0) tempx += height;
            if(tempx >= height) tempx -= height;
            if(board[tempx][tempy]) n++;
          }
        }
        //Don't count the cell itself
        if(board[x][y]) n--;
        newboard[x][y] = (n==3 | (n==2 && board[x][y]));
      }
#ifdef TIMING
      fprintf(fp, "%i %i %f %f\n", timesteps,x, start, omp_get_wtime()-world_start);
#endif
    }
  }
  }else{
    for(int x = 0; x < height; x++){
      /* During the first timestep, the tasks do not depend on previous task completion */
      #pragma omp task firstprivate(x) depend(out:depends[timesteps][x])
      {
#ifdef TIMING
        double start = omp_get_wtime()-world_start;
#endif
      for(int y = 0; y < width; y++){
        int n = 0;
        for(int x1 = x-1; x1 <= x+1; x1++){
          for(int y1 = y-1; y1 <= y+1; y1++){
            int tempx = x1, tempy = y1;
            /* Here we check for the edge cases*/
            tempy = (tempy+width)%width;
            tempx = (tempx+height)%height;

            if(board[tempx][tempy]) n++;
          }
        }
        //Don't count the cell itself
        if(board[x][y]) n--;
        newboard[x][y] = (n==3 | (n==2 && board[x][y]));
      }
#ifdef TIMING
      fprintf(fp, "%i %i %f %f\n", timesteps,x, start, omp_get_wtime()-world_start);
#endif
    }
  }

  }
}

void init_random(char **board, int width, int height){
  for(int x = 0; x < height; x++){
    for(int y = 0; y < width; y++){
      int temp = rand();
      if(temp < (RAND_MAX/10) ){
        board[x][y] = 1;
      }else{
        board[x][y] = 0;
      }
    }
  }
}


void init_block(char **board, int width, int height){
  if(width >= 3 && height >= 3){
    for(int x = 0; x < height; x++){
      for(int y = 0; y < width; y++){
        if((x == 0 || x == 1) && (y == 0 || y == 1)){
          board[x][y] = 1;
        }else{
          board[x][y] = 0;
        }
      }
    }
  }else{
    return;
  }
}

void init_blinker(char **board, int width, int height){
  if(width >=6 && height >=6){
    for(int x = 0; x < height; x++){
      for(int y = 0; y < width; y++){
        if(x==1 && (y > 0 && y < 4)){
          board[x][y] =1;
        }else{
          board[x][y] = 0;
        }
      }
    }
  }else{
    return;
  }
}

void init_glider(char **board, int width, int height){
  if(width >= 10 && height >= 10){
    for(int x = 0; x < height; x++){
      for(int y = 0; y < width; y++){
        if(y == 2 && x < 3){
          board[x][y] = 1;
        }else if(y==1 && x==2){
          board[x][y] = 1;
        }else if(y==0 && x==1){
          board[x][y] = 1;
        }else{
          board[x][y] = 0;
        }
      }
    }
  }
}

void init_spaceship(char **board, int width, int height){
  if(width >= 25 && height >= 25){
    for(int x = 0; x < height; x++){
      for(int y = 0; y < width; y++){
        if(x==0 && (y > 0 && y < 5)){
          board[x][y] = 1;
	}else if( x==1 && (y == 0 || y == 4)){
          board[x][y] = 1;
	}else if( x == 2 && y == 4){
          board[x][y] = 1;
	}else if( x == 3 && (y == 0 || y == 3)){
          board[x][y] = 1;
	}else{
          board[x][y] = 0;
	}
      }
    }
  }
}

int main(int argc, char *argv[]){

  char **board;
  char **board2;
  char **temp;
  int timesteps = TIMESTEPS;
  int width = WIDTH;
  int height = HEIGHT;
  char **depends;
  FILE *fp = fopen("task2.out", "w");
  
  
  depends = malloc(sizeof(char*)*timesteps); 
  board = malloc(sizeof(char*) * height);
  board2 = malloc(sizeof(char*) * height);
  for(int i = 0; i < timesteps; i++){
    depends[i] = malloc(sizeof(char)*height);
  }
  for(int i = 0; i < height; i++){
    board[i] = malloc(sizeof(char)*width);
    board2[i] = malloc(sizeof(char)*width);
  }


  init_random(board, width, height);
  world_start = omp_get_wtime();
  double start = omp_get_wtime();
#pragma omp parallel default(none) shared(board, board2, temp, width, height,depends,timesteps,world_start,fp)
{
#pragma omp single nowait
  {
    for(int i = 0; i < TIMESTEPS; i++){
    evolve_step(board, board2, width, height,i,depends,fp);
    temp = board;
    board = board2;
    board2 = temp;
    }
  }
}
  double finish = omp_get_wtime();
#ifndef TIMING
  printf("Game of life took %f seconds.\n", finish-start);
#endif
  fclose(fp);
  for(int i = 0; i < timesteps; i++){
    free(depends[i]);
  }
  free(depends);
  for(int i = 0; i < height; i++){
    free(board[i]);
    free(board2[i]);
  }
  free(board);
  free(board2);
  return 0;
}
