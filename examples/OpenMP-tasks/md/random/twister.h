#ifndef TWISTER
#define TWISTER

#define MAX_RAND 0xFFFFFFFF
void twister_Initialize(const uint32_t  seed);
uint32_t ExtractU32();
#endif
