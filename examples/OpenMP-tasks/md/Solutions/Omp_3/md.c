#include "md.h"
#include <stdio.h>
#include <omp.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include "twister.h"

#ifdef __GNUC__
#define _mm_malloc(a, b) malloc(a)
#define _mm_free free
#endif

char dependencies[nparts];

void update(struct data *md_data, struct data *new) {
  int i, j;
  double rmass;

  rmass = 1.0 / mass;
  for (i = 0; i < nparts; i += 16) {
#pragma omp task default(none) firstprivate(md_data, new, rmass) private(j)    \
    firstprivate(i) depend(inout : dependencies[i])
    {
      for (j = i; j < nparts && j < i + 16; j++) {
        new->x_pos[j] = md_data->x_pos[j] + md_data->x_vel[j] * dt +
                        0.5 * dt *dt *md_data->x_accel[j];
        new->y_pos[j] = md_data->y_pos[j] + md_data->y_vel[j] * dt +
                        0.5 * dt *dt *md_data->y_accel[j];
        new->z_pos[j] = md_data->z_pos[j] + md_data->z_vel[j] * dt +
                        0.5 * dt *dt *md_data->z_accel[j];

        new->x_vel[j] =
            md_data->x_vel[j] +
            0.5 * dt *(md_data->x_force[j] * rmass + md_data->x_accel[j]);
        new->y_vel[j] =
            md_data->y_vel[j] +
            0.5 * dt *(md_data->y_force[j] * rmass + md_data->y_accel[j]);
        new->z_vel[j] =
            md_data->z_vel[j] +
            0.5 * dt *(md_data->z_force[j] * rmass + md_data->z_accel[j]);

        new->x_accel[j] = md_data->x_force[j] * rmass;
        new->y_accel[j] = md_data->y_force[j] * rmass;
        new->z_accel[j] = md_data->z_force[j] * rmass;
        new->cutoff[j] = md_data->cutoff[j];
      }
    }
  }
}

void compute_step(struct data *md_data, double *restrict t_e) {

  int i, j, k;
  double rxx, ryy, rzz, rrr, rsq, ir, irsq, cutoffs, force, energy;
  double sig_r, sig_r2, sig_r6, sig_r12;
  double t_x, t_y, t_z;
  static double t_t_e;

  // One-sided force interaction
  for (i = 0; i < nparts; i += 16) {
#pragma omp task default(none)                                                 \
    firstprivate(md_data, t_e) private(j, k, t_y, t_z, t_x) firstprivate(i)    \
    depend(inout : dependencies[i]) private(rxx, ryy, rzz, rrr, rsq) private(  \
        cutoffs, force, energy, t_t_e, sig_r, sig_r2, sig_r6, sig_r12)
    {
      for (k = i; k < nparts && k < i + 16; k++) {
        t_x = 0.0;
        t_y = 0.0;
        t_z = 0.0;
        t_t_e = 0.0;
        for (j = 0; j < nparts; j++) {
          if (k == j)
            continue;
          rxx = md_data->x_pos[k] - md_data->x_pos[j];
          ryy = md_data->y_pos[k] - md_data->y_pos[j];
          rzz = md_data->z_pos[k] - md_data->z_pos[j];
          cutoffs = fmaxf(md_data->cutoff[k], md_data->cutoff[j]);
          rsq = rxx * rxx + ryy * ryy + rzz * rzz;
          if (rsq < cutoffs * cutoffs) {
            // Compute interaction
            rrr = sqrt(rsq);
            sig_r = sigma / rrr;
            sig_r2 = sig_r * sig_r;
            sig_r6 = sig_r2 * sig_r2 * sig_r2;
            sig_r12 = sig_r6 * sig_r6;
            force = 4.0 * eps * (sig_r12 - sig_r6);
            energy = 4.0 * eps * sig_r6 * (sig_r6 - 1.0);

            // Store energy due to particle i
            t_t_e += 0.5 * energy;
            t_x += force * rxx;
            t_y += force * ryy;
            t_z += force * rzz;
          }
        }
        md_data->x_force[k] = t_x;
        md_data->y_force[k] = t_y;
        md_data->z_force[k] = t_z;
        t_e[omp_get_thread_num()] += t_t_e;
      }
    }
  }
}

void destruct(struct data *md_data) {

  _mm_free(md_data->x_pos);
  _mm_free(md_data->y_pos);
  _mm_free(md_data->z_pos);
  _mm_free(md_data->x_vel);
  _mm_free(md_data->y_vel);
  _mm_free(md_data->z_vel);
  _mm_free(md_data->x_force);
  _mm_free(md_data->y_force);
  _mm_free(md_data->z_force);
  _mm_free(md_data->x_accel);
  _mm_free(md_data->y_accel);
  _mm_free(md_data->z_accel);
  _mm_free(md_data->cutoff);
}

void initialise(struct data *md_data) {
  int i, j, k;

  // Allocate all of of the arrays.
  int size = (nparts / 8 + 1) * 8;
  md_data->x_pos = _mm_malloc(sizeof(double) * nparts, 64);
  md_data->y_pos = _mm_malloc(sizeof(double) * nparts, 64);
  md_data->z_pos = _mm_malloc(sizeof(double) * nparts, 64);
  md_data->x_vel = _mm_malloc(nparts * sizeof(double),
                              64); // From here on is set to 0 other than cutoff
  md_data->y_vel = _mm_malloc(nparts * sizeof(double), 64);
  md_data->z_vel = _mm_malloc(nparts * sizeof(double), 64);
  md_data->x_force = _mm_malloc(nparts * sizeof(double), 64);
  md_data->y_force = _mm_malloc(nparts * sizeof(double), 64);
  md_data->z_force = _mm_malloc(nparts * sizeof(double), 64);
  md_data->x_accel = _mm_malloc(nparts * sizeof(double), 64);
  md_data->y_accel = _mm_malloc(nparts * sizeof(double), 64);
  md_data->z_accel = _mm_malloc(nparts * sizeof(double), 64);
  md_data->cutoff = _mm_malloc(sizeof(double) * nparts, 64);

  for (i = 0; i < nparts; i++) {
    md_data->x_vel[i] = 0.0;
    md_data->y_vel[i] = 0.0;
    md_data->z_vel[i] = 0.0;
    md_data->x_force[i] = 0.0;
    md_data->y_force[i] = 0.0;
    md_data->z_force[i] = 0.0;
    md_data->x_accel[i] = 0.0;
    md_data->y_accel[i] = 0.0;
    md_data->z_accel[i] = 0.0;
  }

  twister_Initialize(0);

  for (i = 0; i < nparts; i++) {
    md_data->x_pos[i] = ((double)ExtractU32() / (double)MAX_RAND) * box[0];
    md_data->y_pos[i] = ((double)ExtractU32() / (double)MAX_RAND) * box[1];
    md_data->z_pos[i] = ((double)ExtractU32() / (double)MAX_RAND) * box[2];
    md_data->cutoff[i] = ((double)(i + 1)) * (box[0] / (double)nparts);
  }
  md_data->E0 = 0.0;
}

void main() {
  double t1, t;
  struct data *md_data = malloc(sizeof(struct data));
  struct data *new = malloc(sizeof(struct data));
  double t_e[512];

  initialise(md_data);
  initialise(new);
#pragma omp parallel default(none) shared(md_data, new) shared(t_e)
  {
    t_e[omp_get_thread_num()] = 0.0;
#pragma omp single
    { compute_step(md_data, t_e); }
#pragma omp atomic
    md_data->E0 += t_e[omp_get_thread_num()];
  }
  struct data *temp = md_data;
  md_data = new;
  new = temp;

  t = 0.0;
  for (int i = 0; i < nsteps; i++) {
    md_data->E0 = 0.;
    new->E0 = 0.;
    t1 = omp_get_wtime();
#pragma omp parallel default(none) shared(md_data, new) shared(t_e)
    {
      t_e[omp_get_thread_num()] = 0.0;
#pragma omp single
      {
        compute_step(md_data, t_e);
        update(md_data, new);
      }
#pragma omp atomic
      md_data->E0 += t_e[omp_get_thread_num()];
    }
    t += omp_get_wtime() - t1;
    printf("Energy: %f\n", md_data->E0);
    struct data *temp = md_data;
    md_data = new;
    new = temp;
  }

  printf("Runtime for loop iterations:%f s\n", t);

  destruct(md_data);
  destruct(new);
  free(md_data);
  free(new);
}
