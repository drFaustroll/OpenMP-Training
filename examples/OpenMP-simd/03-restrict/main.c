#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "loop.h"

#define N 1000000

REAL get_random() { return (REAL)rand() / (REAL)RAND_MAX; }

int main (void) {
  REAL * a;
  REAL * x;
  REAL * y;
  double start, end;

#ifdef RESTRICT
  printf("With restrict keyword\n");
#else
  printf("Without restrict keyword\n");
#endif

  /* Allocate aligned memory for arrays */
  a = (REAL *) _mm_malloc(N * sizeof(REAL), 64);
  x = (REAL *) _mm_malloc(N * sizeof(REAL), 64);
  y = (REAL *) _mm_malloc(N * sizeof(REAL), 64);

  /* Check for NULLs */
  if (a == NULL || x == NULL || y == NULL) exit(-1);

  /* Initialise x and y */
  srand(100); /* seed RNG */
  for (int i = 0; i < N; i++) {
    a[i] = get_random();
    x[i] = get_random();
    y[i] = get_random();
  }

  /* Run the main computation */
  start = omp_get_wtime();
  do_work(N, a, x, y);
  end = omp_get_wtime();
  printf("Execution time: %.12lfs\n", end-start);

  /* Free allocated aligned memory */
  _mm_free(a);
  _mm_free(x);
  _mm_free(y);

  return 0;
}
