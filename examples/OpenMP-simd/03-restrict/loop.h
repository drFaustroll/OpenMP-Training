#ifndef loop_h
#define loop_h

#define REAL float
#ifdef RESTRICT
  void do_work(int, REAL*restrict, REAL*restrict, REAL*restrict);
#else
  void do_work(int, REAL*, REAL*, REAL*);
#endif
#endif
