Numerical Libraries using Intel MKL
************************************

Setting up
==========


We shall start by setting up the environment and copy the needed files for the lab

.. code-block:: bash

   ssh trnxy-train@hcxlogin1.hartree.stfc.ac.uk
   -bash-4.2$ cp -r ../shared/nllab .
  
Check the environment is correctly set-up. Inquire for compilers version and check that $MKLROOT variable is set.

.. code-block:: bash

  -bash-4.2$ module load intel
  -bash-4.2$ module list

  Currently Loaded Modules:
  1) use.scafellpike   2) xalt/1.1.2   3) StdEnv   4) licenses/intel   5) intel/18.0.128

  -bash-4.2$ echo $MKLROOT 
   /lustre/scafellpike/local/apps/intel/intel_cs/2018.0.128/mkl

Matrix - Matrix Multiplication: Naïve approach
==============================================

Consider two real matrices :math:`A\in R^{n\!\times\! k}` and :math:`B\in R^{k\!\times\!m}`, with n,k,m positive integers.
The product of :math:`AB` is give by the formula

.. math:: 

  C_{i,j}=\sum_{l=1}^{k}A_{i,l}B_{l,j}

Choose from folder **task01** the version written in your favourite programming language C or Fortran and try to build it.

Fortran build

.. code-block:: bash

  ifort -o mmm.X flops.F90 mmm.F90 -O3 -axMIC-AVX512 -qopenmp -mkl


C build

.. code-block:: bash

  icc -o mmm.X mmm.c -O3 -axMIC-AVX512  -qopenmp -mkl


Inspect the code, you will notice that the only call to MKL is the timing routine. Regardless of the version you choose running the executable shall show you something like this

.. code-block:: bash

  sancho02@dapple02:~/code/task01/c$ ./mmm.X
  Wrong number of arguments!!! 
  usage: ./mmm.X  nMatrix kMatrix mMatrix NSamples

and with the right parameters

.. code-block:: bash

  amelena@login:/work/knl-users/amelena/code/task01/c> ./mmm.X 500 500 500 3 
  Using 16 threads ouf of a maximum of 16
  Running... ./mmm.X 500 500 500 3 
  Matrix A sizes 500x500. Memory requested for 250000 elements, that is 1.86264515E-03 GiB
  Matrix B sizes 500x500. Memory requested for 250000 elements, that is 1.86264515E-03 GiB
  Matrix C sizes 500x500. Memory requested for 250000 elements, that is 1.86264515E-03 GiB
  #Loop | Sizes n,k,m        |   Time (s)|  GFlop/s
     1    500    500    500      0.2221      1.1256
     2    500    500    500      0.1602      1.5607
     3    500    500    500      0.1573      1.5897

  The (1:4,1:4) block of the result:
  -5.60813625e+02 -5.59129500e+02 -5.57445375e+02 -5.55761250e+02 
  -5.59129500e+02 -5.57452125e+02 -5.55774750e+02 -5.54097375e+02 
  -5.57445375e+02 -5.55774750e+02 -5.54104125e+02 -5.52433500e+02 
  -5.55761250e+02 -5.54097375e+02 -5.52433500e+02 -5.50769625e+02 

  Summary:
  #Sizes n,k,m        |  Avg. Time (s) |   Avg. Gflop/s |   Min. GFlop/s |   Max. GFlop/s | σ GFlop/s
     500    500    500       0.17985122       1.39003784       1.12556629       1.58970265       0.21521164

Running on KNL one will need to employ a script as shown bellow (job.pbs in your task folder). You shall be familiar by now with submitting to the queue.


.. code-block:: bash

  #!/bin/bash --login
  
  #PBS -N mycoolname
  #PBS -l select=1:aoe=quad_100
  #PBS -l walltime=00:10:00
  #PBS -A k01-$USER

  module load intel
  export OMP_NUM_THREADS=1
  export MKL_NUM_THREADS=$OMP_NUM_THREADS
  ./mmm.X 500 500 500 3 

+ What is the theoretical double precision GFLOP/s for a KNL, what about the skylake CPU you are using?.
+ Build your code with no optimisation (-O0) and run on one thread (use OMP_NUM_THREADS)
+ Build your code with optimisation (-O3 -axMIC-AVX512) and run. How do the two versions compare?
+ Run the optimised binary on different number of threads,1,2,4,8,16. What is the maximum GFlop/s you get.
+ Rerun the code above for different matrix sizes. Consider 500 and 1000 and vary number of threads.
+ How do you compare the performance you obtained with the theoretical maximum for your CPU?
+ Check the code and try to see why you get the performance you get.

.. table:: Results for matrix matrix multiplication

   +-------------+---+---+---+---+----+
   | Thread/Size | 1 | 2 | 4 | 8 | 16 |
   +=============+===+===+===+===+====+
   | 500         |   |   |   |   |    |
   +-------------+---+---+---+---+----+
   | 1000        |   |   |   |   |    |
   +-------------+---+---+---+---+----+

Optimised version
=================

BLAS/LAPACK are standard libraries that provide optimised linear algebra. Specific vendor optimised versions of these libraries
are provided by commercial vendors. In this exercise we use MKL from Intel (Math Kernel Library). Fortran (flops.F90 and dgemm.F90) and C(flops.h and dgemm.c)
implementations of a call to DGEMM are provided in folder **task02**.

Fortran version

.. code-block:: bash

  ifort -o dgemm.X -O3 -axMIC-AVX512 flops.F90 dgemm.F90 -qopenmp
  flops.F90(14): (col. 27) remark: flops_mp_flopsdgetrf_ has been targeted for automatic cpu dispatch
  flops.F90(21): (col. 27) remark: flops_mp_mulgetrf_ has been targeted for automatic cpu dispatch
  flops.F90(31): (col. 27) remark: flops_mp_addgetrf_ has been targeted for automatic cpu dispatch
  flops.F90(43): (col. 27) remark: flops_mp_flopsdgetri_ has been targeted for automatic cpu dispatch
  flops.F90(50): (col. 27) remark: flops_mp_mulgetri_ has been targeted for automatic cpu dispatch
  flops.F90(57): (col. 27) remark: flops_mp_addgetri_ has been targeted for automatic cpu dispatch
  flops.F90(63): (col. 27) remark: flops_mp_flopszgetrf_ has been targeted for automatic cpu dispatch
  flops.F90(71): (col. 27) remark: flops_mp_flopszgetri_ has been targeted for automatic cpu dispatch
  dgemm.F90(83): (col. 9) remark: MAIN__ has been targeted for automatic cpu dispatch
  dgemm.F90(38): (col. 14) remark: util_mp_creatematrix_ has been targeted for automatic cpu dispatch
  dgemm.F90(24): (col. 14) remark: util_mp_initmatrix_ has been targeted for automatic cpu dispatch
  /tmp/ifortsJnEdM.o: In function `MAIN__.Z':
  dgemm.F90:(.text+0x9ff): undefined reference to `dgemm_'
  dgemm.F90:(.text+0xc8d): undefined reference to `dgemm_'
  /tmp/ifortsJnEdM.o: In function `MAIN__.A':
  dgemm.F90:(.text+0x4fa4): undefined reference to `dgemm_'
  dgemm.F90:(.text+0x5237): undefined reference to `dgemm_'


  ifort -o dgemm.X -O3 -axMIC-AVX512 flops.F90 dgemm.F90 -qopenmp -mkl=parallel
  ifort -o dgemm.X -O3 -axMIC-AVX512 flops.F90 dgemm.F90 -qopenmp -L$MKLROOT/lib/intel64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core

  ifort -o dgemm.X -O3 -axMIC-AVX512 flops.F90 dgemm.F90 -mkl=sequential
  ifort -o dgemm.X -O3 -axMIC-AVX512 flops.F90 dgemm.F90 -L$MKLROOT/lib/intel64 -lmkl_intel_lp64 -lmkl_sequential -lmkl_core

 C version

.. code-block:: bash
  
  icc -o dgemm.X dgemm.c -O3 -axMIC-AVX512 -qopenmp
  dgemm.c(13): (col. 33) remark: main has been targeted for automatic cpu dispatch
  dgemm.c(115): (col. 48) remark: initMatrix has been targeted for automatic cpu dispatch
  /tmp/iccBxjRUl.o: In function `main.Z':
  dgemm.c:(.text+0xeea): undefined reference to `DGEMM'
  dgemm.c:(.text+0x19a9): undefined reference to `dsecnd'
  dgemm.c:(.text+0x1a21): undefined reference to `DGEMM'
  dgemm.c:(.text+0x1a2a): undefined reference to `dsecnd'
  /tmp/iccBxjRUl.o: In function `main.A':
  dgemm.c:(.text+0x344a): undefined reference to `DGEMM'
  dgemm.c:(.text+0x3d35): undefined reference to `dsecnd'
  dgemm.c:(.text+0x3db5): undefined reference to `DGEMM'
  dgemm.c:(.text+0x3dbe): undefined reference to `dsecnd'

  icc -o dgemm.X dgemm.c -O3 -axMIC-AVX512 -qopenmp -mkl=parallel
  icc -o dgemm.X dgemm.c -O3 -axMIC-AVX512 -qopenmp -L$MKLROOT/lib/intel64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core

  icc -o dgemm.X dgemm.c -O3 -axMIC-AVX512 -qopenmp -mkl=sequential
  icc -o dgemm.X dgemm.c -O3 -axMIC-AVX512 -qopenmp -L$MKLROOT/lib/intel64 -lmkl_intel_lp64 -lmkl_sequential -lmkl_core



**Linking** against MKL with gfortran or gcc
--------------------------------------------

.. code-block:: bash
  
  module load gcc7/7.2.0
  gfortran -o dgemm.X -O3 flops.F90 dgemm.F90 -fopenmp -L$MKLROOT/lib/intel64 -lmkl_gf_lp64 -lmkl_gnu_thread -lmkl_core

  gcc -o dgemm.X dgemm.c -fopenmp -O3 -fopenmp -L$MKLROOT/lib/intel64 -lmkl_gf_lp64 -lmkl_gnu_thread -lmkl_core -lm

Script for running on KNL using only physical cores

.. code-block:: bash

  export OMP_NUM_THREADS=64
  export MKL_NUM_THREADS=$OMP_NUM_THREADS
  ./dgemm.X 3000 3000 3000 10


for gcc do not forget to load the gcc module used to build the executable.

+ Which executable does give better time intel or gfortran one?

Running on different number of threads
--------------------------------------

.. code-block:: bash

  Using 64 threads out of a maximum of 64
  Running... ./dgemm.X 3000 3000 3000 10 
  Matrix A sizes 3000x3000. Memory requested for 9000000 elements, that is   6.70552254E-02 GiB
  Matrix B sizes 3000x3000. Memory requested for 9000000 elements, that is   6.70552254E-02 GiB
  Matrix C sizes 3000x3000. Memory requested for 9000000 elements, that is   6.70552254E-02 GiB
  #Loop |        Sizes n,k,m |       Time (s) |     Gflop/s   |  
     1   3000   3000   3000       0.03228712    1672.49350918
     2   3000   3000   3000       0.03253007    1660.00260919
     3   3000   3000   3000       0.03246903    1663.12307523
     4   3000   3000   3000       0.03271222    1650.75919974
     5   3000   3000   3000       0.03234100    1669.70700637
     6   3000   3000   3000       0.03271389    1650.67498470
     7   3000   3000   3000       0.03260088    1656.39702204
     8   3000   3000   3000       0.03224301    1674.78142816
     9   3000   3000   3000       0.03236508    1668.46471061
    10   3000   3000   3000       0.03206897    1683.87084687
 
  The (1:4,1:4) block of the result: 
  -3.74812521E+04 -3.74625083E+04 -3.74437646E+04 -3.74250208E+04 
  -3.74625083E+04 -3.74437771E+04 -3.74250458E+04 -3.74063146E+04 
  -3.74437646E+04 -3.74250458E+04 -3.74063271E+04 -3.73876083E+04 
  -3.74250208E+04 -3.74063146E+04 -3.73876083E+04 -3.73689021E+04 
 
  Summary: 
    #Sizes n,k,m     |  Avg. Time (s) |   Avg. GFlop/s | Min. GFlop/s   | Max. GFlop/s   |    σ GFlop/s   
   3000   3000   3000       0.03243313    1664.96427374    1650.67498470    1683.87084687      10.25984807

+ Generate binaries for each implementation. Be sure you linked against the threaded version of MKL.
+ Consider we multiply square matrices. What is the maximum size that can be used?
+ Run the programme for different matrix sizes -- 500,1000,2000,3000,4000,5000
+ Repeat the above step for different number of threads 1,2,4,8,16,32,64
+ Determine from above at which size of the matrix one reaches the highest performance for a fixed thread count.

.. table:: Results for optimised matrix matrix multiplication

   +-------------+---+---+---+---+----+----+----+
   | Thread/Size | 1 | 2 | 4 | 8 | 16 | 32 | 64 |
   +=============+===+===+===+===+====+====+====+
   | 500         |   |   |   |   |    |    |    |
   +-------------+---+---+---+---+----+----+----+
   | 1000        |   |   |   |   |    |    |    |
   +-------------+---+---+---+---+----+----+----+
   | 2000        |   |   |   |   |    |    |    |
   +-------------+---+---+---+---+----+----+----+
   | 3000        |   |   |   |   |    |    |    |
   +-------------+---+---+---+---+----+----+----+
   | 4000        |   |   |   |   |    |    |    |
   +-------------+---+---+---+---+----+----+----+
   | 5000        |   |   |   |   |    |    |    |
   +-------------+---+---+---+---+----+----+----+


FFT 1D/3D
=========

In the folders **task03** and **task04** you will find simple examples in C and Fortran of calling a multithreaded 1D and 3D Fourier transform.

Fortran build/C Build


.. code-block:: bash

  ifort -o fftw1D.X fftw1D.F90 -O3 -axMIC-AVX512 -mkl=parallel -qopenmp -I$MKLROOT/include/fftw
  ifort -o fftw3D.X fftw3D.F90 -O3 -axMIC-AVX512 -mkl=parallel -qopenmp -I$MKLROOT/include/fftw

  icc -o fftw1D.X fftw1D.c -O3 -axMIC-AVX512 -mkl=parallel -qopenmp
  icc -o fftw3D.X fftw3D.c -O3 -axMIC-AVX512 -mkl=parallel -qopenmp

Running the code with the following script

.. code-block:: bash

  #PBS -N mycoolname
  #PBS -l select=1:aoe=quad_100
  #PBS -l walltime=00:10:00
  #PBS -A k01-$USER

  export PBS_O_WORKDIR=$(readlink -f $PBS_O_WORKDIR)               

  cd $PBS_O_WORKDIR
  module swap PrgEnv-cray/6.0.4 PrgEnv-intel/6.0.4

  export OMP_NUM_THREADS=64
  export MKL_NUM_THREADS=$OMP_NUM_THREADS
  ./fftw1D.X 100000 10 T

shall hold something like

.. code-block:: bash

  Running with N=100000
  Size of sample is 10
  Scaling is On                              
  Needs to allocate   0.44703484E-02 GiB
  Setting up 64 threads                        
  Crunching...
  #Loop  |     Size |       Time (s) 
       1     100000   0.40901995    
       2     100000   0.82206726E-03
       3     100000   0.72789192E-03
       4     100000   0.73599815E-03
       5     100000   0.73909760E-03
       6     100000   0.78988075E-03
       7     100000   0.73289871E-03
       8     100000   0.73099136E-03
       9     100000   0.73599815E-03
      10     100000   0.73218346E-03
  Accuracy test:  -0.82220133E-10
  #Size     | Min. Time (s) | Max. Time (s) | Avg. Time (s) | Total time (s)
    100000      0.7279E-03      0.4090          0.7524E-03      0.4158    
    #Size|    GFlops min |    GFlops max |      GFlops avg
    100000      0.4061E-01       22.82           22.08 

+ Experience with different sizes for both 1D and 3D transforms. Try 1000, 10000, 100000, 10000000, 20000000 for 1D.
+ For 3D, use 128, 256, 512 and no scaling in both cases.
+ Repeat the above for 1, 2, 4, 8, 16, 32, 64 threads.

Results for 1D

.. table::  Results for 1D

   +-------------+---+---+---+---+----+----+----+
   | Thread/Size | 1 | 2 | 4 | 8 | 16 | 32 | 64 |
   +=============+===+===+===+===+====+====+====+
   | 1000        |   |   |   |   |    |    |    |
   +-------------+---+---+---+---+----+----+----+
   | 10000       |   |   |   |   |    |    |    |
   +-------------+---+---+---+---+----+----+----+
   | 100000      |   |   |   |   |    |    |    |
   +-------------+---+---+---+---+----+----+----+
   | 10000000    |   |   |   |   |    |    |    |
   +-------------+---+---+---+---+----+----+----+
   | 20000000    |   |   |   |   |    |    |    |
   +-------------+---+---+---+---+----+----+----+



.. table:: Results for 3D

   +-------------+---+---+---+---+----+----+----+
   | Thread/Size | 1 | 2 | 4 | 8 | 16 | 32 | 64 |
   +=============+===+===+===+===+====+====+====+
   | 128         |   |   |   |   |    |    |    |
   +-------------+---+---+---+---+----+----+----+
   | 256         |   |   |   |   |    |    |    |
   +-------------+---+---+---+---+----+----+----+
   | 512         |   |   |   |   |    |    |    |
   +-------------+---+---+---+---+----+----+----+


Linking against fftw with Intel Compilers
-----------------------------------------

.. code-block:: bash

  module load fftw3/3.3.6
  icc -o fftw1D.X fftw1D.c -O3 -axMIC-AVX512 -qopenmp -L$FFTW_DIR -lfftw3 -lfftw3_omp  -I$FFTW_INC
  ifort -o fftw1D.X fftw1D.F90 -O3 -axMIC-AVX512 -qopenmp -L$FFTW_DIR -lfftw3 -lfftw3_omp  -I$FFTW_INC


Redo the tasts from the previous exercise. Which library offers you better performance on KNL?

Extras
======

+ Redo the same exercises but now running on the skylake rather than KNL
+ Choose an exercise of your choice and experiment with openmpi affinity. Which is the best affinity?

Selected Results
================
    
Results for different tasks for reference only. All numbers are reported in GFlop/s

.. code-block:: none


    task 01
    Fortran

    | Threads      |        |        |        |         |         |
    !  Size        | 1      | 2      | 4      | 8       | 16      |
    |--------------|--------|--------|--------|---------|---------|
    |  500         |   5.03 |   5.05 |   5.06 |    5.05 |    5.00 |
    | 1000         |   8.28 |   8.46 |   8.46 |    8.46 |    8.46 |


    | Threads |         |         |         |         |         |
    !  Size   | 1       | 2       |  4      | 8       | 16      |
    |---------|---------|---------|---------|---------|---------|
    |     500 |    0.12 |    0.12 |    0.12 |    0.12 |    0.12 |
    |    1000 |    0.11 |    0.11 |    0.11 |    0.11 |    0.11 |

    Task02
    Fortran


    | Threads |         |         |         |         |         |         |         |
    | Size    |  1      |  2      |  4      | 8       | 16      |  32     |  64     |
    |---------|---------|---------|---------|---------|---------|---------|---------|
    |     500 |   27.48 |   49.23 |   80.43 |  148.33 |  245.22 |  361.06 |  248.86 |
    |    1000 |   31.65 |   60.02 |  109.91 |  184.42 |  341.41 |  598.16 |  748.86 |
    |    2000 |   32.99 |   57.96 |  122.24 |  240.77 |  474.51 |  842.56 | 1424.85 |
    |    3000 |   33.41 |   59.37 |  125.45 |  250.09 |  493.58 |  946.67 | 1613.62 |
    |    4000 |   33.56 |   59.38 |  126.49 |  251.84 |  500.21 |  984.71 | 1819.47 |
    |    5000 |   33.57 |   59.93 |  127.90 |  256.11 |  508.25 | 1003.95 | 1930.31 |


    C
    | Threads |         |         |         |         |         |         |         |
    | Size    |  1      |  2      |  4      | 8       | 16      |  32     |  64     |
    |---------|---------|---------|---------|---------|---------|---------|---------|
    |     500 |   27.54 |   49.36 |   80.04 |  146.33 |  253.11 |  348.22 |  253.51 |
    |    1000 |   31.63 |   59.90 |  109.61 |  183.99 |  341.08 |  595.62 |  796.93 |
    |    2000 |   32.99 |   57.93 |  120.43 |  240.63 |  474.34 |  842.47 | 1411.36 |
    |    3000 |   33.41 |   59.24 |  125.34 |  250.08 |  493.45 |  947.08 | 1613.41 |
    |    4000 |   33.55 |   59.43 |  126.90 |  252.14 |  500.24 |  984.17 | 1810.59 |
    |    5000 |   33.63 |   60.18 |  128.38 |  256.53 |  507.82 | 1002.93 | 1936.52 |

    Task03

    Fortran
    | Threads |        |        |        |        |        |        |        |
    | Size    |     1  |  2     |    4   |   8    |   16   |   32   |   64   |
    |---------|--------|--------|--------|--------|--------|--------|--------|
    |    1000 |   5.08 |   5.04 |   5.01 |   5.01 |   5.04 |   5.07 |   4.98 |
    |   10000 |   4.30 |   6.95 |   9.92 |  13.92 |  20.36 |  31.80 |  30.62 |
    |  100000 |   2.82 |   4.84 |   9.13 |  16.96 |  28.75 |  50.96 |  68.81 |
    | 1000000 |   2.32 |   4.37 |   8.84 |  17.28 |  33.77 |  65.94 | 123.30 |
    | 2000000 |   2.06 |   3.71 |   7.38 |  14.42 |  29.14 |  55.63 |  93.11 |



    C
    | Threads |        |        |        |        |        |        |        |
    | Size    |     1  |  2     |    4   |   8    |   16   |   32   |   64   |
    |---------|--------|--------|--------|--------|--------|--------|--------|
    |    1000 |   4.98 |   4.95 |   5.04 |   4.95 |   4.95 |   4.98 |   5.01 |
    |   10000 |   4.34 |   6.70 |  10.82 |  13.66 |  24.38 |  30.92 |  33.42 |
    |  100000 |   2.81 |   4.82 |   9.05 |  16.72 |  29.12 |  48.67 |  75.89 |
    | 1000000 |   2.36 |   4.38 |   8.85 |  17.25 |  33.84 |  65.86 | 108.01 |
    | 2000000 |   2.06 |   3.72 |   7.32 |  14.37 |  29.08 |  54.59 |  97.20 |

    Task 04

    Fortran

    | Threads |        |        |        |        |        |        |        |
    | Size    |     1  |  2     |    4   |   8    |   16   |   32   |   64   |
    |---------|--------|--------|--------|--------|--------|--------|--------|
    |     128 |   4.55 |   7.98 |  15.64 |  30.34 |  57.33 |  99.32 | 123.20 |
    |     256 |   3.52 |   5.66 |  10.52 |  19.40 |  33.30 |  60.30 |  92.33 |
    |     512 |   1.65 |   3.10 |   6.09 |  12.07 |  23.64 |  43.44 |  69.45 |

    C
    | Threads |        |        |        |        |        |        |        |
    | Size    |     1  |  2     |    4   |   8    |   16   |   32   |   64   |
    |---------|--------|--------|--------|--------|--------|--------|--------|
    |     128 |   4.57 |   7.86 |  15.44 |  29.99 |  57.55 |  98.84 | 133.23 |
    |     256 |   3.48 |   5.67 |  10.73 |  18.85 |  33.73 |  60.23 |  91.66 |
    |     512 |   1.51 |   3.08 |   6.11 |  12.14 |  23.00 |  43.85 |  71.54 |
